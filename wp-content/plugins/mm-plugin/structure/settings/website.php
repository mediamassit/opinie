<?php

if(function_exists('acf_add_options_page')) {
	$page = acf_add_options_page(apply_filters('mm/acf/add-options-page/mm-website', array(
		'page_title' => __( 'Ustawienia witryny', 'mm-plugin' ),
		'menu_title' => __( 'Witryna', 'mm-plugin' ),
		'menu_slug' => 'mm-website',
		'capability' => 'edit_posts',
		'icon_url' => 'dashicons-editor-paste-word',
		'position' => 50,
		'redirect' => false
	)));
}


if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_5877750476176',
	'title' => 'Social',
	'fields' => array (
		array (
			'key' => 'field_587775f7f7166',
			'label' => 'Social',
			'name' => 'social',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => 'field_58777631f7168',
			'label' => 'You Tube',
			'name' => 'yt',
			'type' => 'url',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
		),
		array (
			'key' => 'field_58777660f7169',
			'label' => 'Facebook',
			'name' => 'fb',
			'type' => 'url',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
		),
		array (
			'key' => 'field_5877766df716a',
			'label' => 'Video Relacjie',
			'name' => 'video',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'mm-website',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;