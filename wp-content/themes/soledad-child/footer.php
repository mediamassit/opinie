<?php
/**
 * This is footer template of Soledad theme
 *
 * @package Wordpress
 * @since   1.0
 */
?>
<!-- END CONTAINER -->
</div>
<div class="clear-footer"></div>
<!--newsletter area-->
<div class="newsletter">
	<div class="container">
		<div class="position">
			<img class="newsletter_img" src="<?php echo get_stylesheet_directory_uri();?>/images/mail.png" alt="">
			<h3 class="newsletter_text">ZAPISZ SIĘ DO NEWSLETTERA</h3>
			<input class="newsletter_input" type="text" placeholder="Wpisz swój adres Email">
			<input class="newsletter_submit" type="submit" value="Wyślij">
		</div>
	</div>
</div>
<!--. newsletter area-->
<?php if ( get_theme_mod( 'penci_sidebar_sticky' ) ): echo '<div id="penci-end-sidebar-sticky"></div>'; endif; ?>

<?php if ( ! get_theme_mod( 'penci_footer_widget_area' ) && ( is_active_sidebar( 'footer-1' ) || is_active_sidebar( 'footer-2' ) || is_active_sidebar( 'footer-3' ) ) ) : ?>
	<div id="widget-area">
	<div class="container">
	<div class="footer-widget-wrapper">
	<?php /* Widgetised Area */
if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'footer-1' ) ) ?>
	</div>
	<div class="footer-widget-wrapper">
<?php /* Widgetised Area */
if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'footer-2' ) ) ?>
	</div>
	<div class="footer-widget-wrapper last">
<?php /* Widgetised Area */
	display_main_categories(get_the_id());
	if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'footer-3' ) ) ?>
		</div>
		</div>
		</div>
<?php endif; ?>

<?php if ( is_active_sidebar( 'footer-instagram' ) ): ?>
	<div class="footer-instagram">
		<?php dynamic_sidebar( 'footer-instagram' ); ?>
	</div>
<?php endif; ?>

<footer id="footer-section">
	<div class="container">
		<?php if ( ! get_theme_mod( 'penci_footer_social' ) ) : ?>
			<?php if ( get_theme_mod( 'penci_email_me' ) || get_theme_mod( 'penci_vk' ) || get_theme_mod( 'penci_facebook' ) || get_theme_mod( 'penci_twitter' ) || get_theme_mod( 'penci_google' ) || get_theme_mod( 'penci_instagram' ) || get_theme_mod( 'penci_pinterest' ) || get_theme_mod( 'penci_linkedin' ) || get_theme_mod( 'penci_flickr' ) || get_theme_mod( 'penci_behance' ) || get_theme_mod( 'penci_tumblr' ) || get_theme_mod( 'penci_youtube' ) || get_theme_mod( 'penci_rss' ) ) : ?>
				<div class="footer-socials-section">
					<ul class="footer-socials">
						<?php if ( get_theme_mod( 'penci_facebook' ) ) : ?>
							<li><a href="<?php echo esc_attr( get_theme_mod( 'penci_facebook' ) ); ?>" target="_blank"><i class="fa fa-facebook"></i><span><?php esc_html_e( 'Facebook', 'soledad' ); ?></span></a></li>
						<?php endif; ?>
						<?php if ( get_theme_mod( 'penci_twitter' ) ) : ?>
							<li><a href="<?php echo esc_attr( get_theme_mod( 'penci_twitter' ) ); ?>" target="_blank"><i class="fa fa-twitter"></i><span><?php esc_html_e( 'Twitter', 'soledad' ); ?></span></a></li>
						<?php endif; ?>
						<?php if ( get_theme_mod( 'penci_google' ) ) : ?>
							<li><a href="<?php echo esc_attr( get_theme_mod( 'penci_google' ) ); ?>" target="_blank"><i class="fa fa-google-plus"></i><span><?php esc_html_e( 'Google +', 'soledad' ); ?></span></a></li>
						<?php endif; ?>
						<?php if ( get_theme_mod( 'penci_instagram' ) ) : ?>
							<li><a href="<?php echo esc_attr( get_theme_mod( 'penci_instagram' ) ); ?>" target="_blank"><i class="fa fa-instagram"></i><span><?php esc_html_e( 'Instagram', 'soledad' ); ?></span></a></li>
						<?php endif; ?>
						<?php if ( get_theme_mod( 'penci_pinterest' ) ) : ?>
							<li><a href="<?php echo esc_attr( get_theme_mod( 'penci_pinterest' ) ); ?>" target="_blank"><i class="fa fa-pinterest"></i><span><?php esc_html_e( 'Pinterest', 'soledad' ); ?></span></a></li>
						<?php endif; ?>
						<?php if ( get_theme_mod( 'penci_linkedin' ) ) : ?>
							<li><a href="<?php echo esc_url( get_theme_mod( 'penci_linkedin' ) ); ?>" target="_blank"><i class="fa fa-linkedin"></i><span><?php esc_html_e( 'Linkedin', 'soledad' ); ?></span></a></li>
						<?php endif; ?>
						<?php if ( get_theme_mod( 'penci_flickr' ) ) : ?>
							<li><a href="<?php echo esc_attr( get_theme_mod( 'penci_flickr' ) ); ?>" target="_blank"><i class="fa fa-flickr"></i><span><?php esc_html_e( 'Flickr', 'soledad' ); ?></span></a></li>
						<?php endif; ?>
						<?php if ( get_theme_mod( 'penci_behance' ) ) : ?>
							<li><a href="<?php echo esc_attr( get_theme_mod( 'penci_behance' ) ); ?>" target="_blank"><i class="fa fa-behance"></i><span><?php esc_html_e( 'Behance', 'soledad' ); ?></span></a></li>
						<?php endif; ?>
						<?php if ( get_theme_mod( 'penci_tumblr' ) ) : ?>
							<li><a href="<?php echo esc_attr( get_theme_mod( 'penci_tumblr' ) ); ?>" target="_blank"><i class="fa fa-tumblr"></i><span><?php esc_html_e( 'Tumblr', 'soledad' ); ?></span></a></li>
						<?php endif; ?>
						<?php if ( get_theme_mod( 'penci_youtube' ) ) : ?>
							<li><a href="<?php echo esc_attr( get_theme_mod( 'penci_youtube' ) ); ?>" target="_blank"><i class="fa fa-youtube-play"></i><span><?php esc_html_e( 'Youtube', 'soledad' ); ?></span></a></li>
						<?php endif; ?>
						<?php if ( get_theme_mod( 'penci_email_me' ) ) : ?>
							<li><a href="<?php echo get_theme_mod( 'penci_email_me' ); ?>"><i class="fa fa-envelope-o"></i><span><?php esc_html_e( 'Email', 'soledad' ); ?></span></a></li>
						<?php endif; ?>
						<?php if ( get_theme_mod( 'penci_vk' ) ) : ?>
							<li><a href="<?php echo esc_attr( get_theme_mod( 'penci_vk' ) ); ?>" target="_blank"><i class="fa fa-vk"></i><span><?php esc_html_e( 'Vk', 'soledad' ); ?></span></a></li>
						<?php endif; ?>
						<?php if ( get_theme_mod( 'penci_bloglovin' ) ) : ?>
							<li><a href="<?php echo esc_attr( get_theme_mod( 'penci_bloglovin' ) ); ?>" target="_blank"><i class="fa fa-heart"></i><span><?php esc_html_e( 'Bloglovin', 'soledad' ); ?></span></a></li>
						<?php endif; ?>
						<?php if ( get_theme_mod( 'penci_vine' ) ) : ?>
							<li><a href="<?php echo esc_attr( get_theme_mod( 'penci_vine' ) ); ?>" target="_blank"><i class="fa fa-vine"></i><span><?php esc_html_e( 'Vine', 'soledad' ); ?></span></a></li>
						<?php endif; ?>
						<?php if ( get_theme_mod( 'penci_soundcloud' ) ) : ?>
							<li><a href="<?php echo esc_attr( get_theme_mod( 'penci_soundcloud' ) ); ?>" target="_blank"><i class="fa fa-soundcloud"></i><span><?php esc_html_e( 'Soundcloud', 'soledad' ); ?></span></a></li>
						<?php endif; ?>
						<?php if ( get_theme_mod( 'penci_snapchat' ) ) : ?>
							<li><a href="<?php echo esc_attr( get_theme_mod( 'penci_snapchat' ) ); ?>" target="_blank"><i class="fa fa-snapchat-ghost"></i><span><?php esc_html_e( 'Snapchat', 'soledad' ); ?></span></a></li>
						<?php endif; ?>
						<?php if ( get_theme_mod( 'penci_spotify' ) ) : ?>
							<li><a href="<?php echo esc_attr( get_theme_mod( 'penci_spotify' ) ); ?>" target="_blank"><i class="fa fa-spotify"></i><span><?php esc_html_e( 'Spotify', 'soledad' ); ?></span></a></li>
						<?php endif; ?>
						<?php if ( get_theme_mod( 'penci_rss' ) ) : ?>
							<li><a href="<?php echo esc_url( get_theme_mod( 'penci_rss' ) ); ?>" target="_blank"><i class="fa fa-rss"></i><span><?php esc_html_e( 'RSS', 'soledad' ); ?></span></a></li>
						<?php endif; ?>
					</ul>
				</div>
			<?php endif; ?>
		<?php endif; ?>
		<?php if ( ( get_theme_mod( 'penci_footer_logo' ) && ! get_theme_mod( 'penci_hide_footer_logo' ) ) || get_theme_mod( 'penci_footer_copyright' ) || ! get_theme_mod( 'penci_go_to_top' ) || get_theme_mod( 'penci_footer_menu' ) ) : ?>
			<div class="footer-logo-copyright<?php if ( ! get_theme_mod( 'penci_footer_logo' ) || get_theme_mod( 'penci_hide_footer_logo' ) ) : echo ' footer-not-logo'; endif; ?><?php if ( get_theme_mod( 'penci_go_to_top' ) ) : echo ' footer-not-gotop'; endif; ?>">
				<?php if ( get_theme_mod( 'penci_footer_logo' ) && ! get_theme_mod( 'penci_hide_footer_logo' ) ) : ?>
					
					<div id="footer-logo">
						<a href="<?php echo esc_url( home_url('/') ); ?>">
							<img src="<?php echo esc_url( get_theme_mod( 'penci_footer_logo' ) ); ?>" alt="<?php esc_html_e( 'Footer Logo', 'soledad' ); ?>" />
						</a>
					</div>
				<?php endif; ?>

				<?php if( get_theme_mod( 'penci_footer_menu' ) ): ?>
					<div class="footer-menu-wrap">
					<?php
					/**
					 * Display main navigation
					 */
					wp_nav_menu( array(
						'container'      => false,
						'theme_location' => 'footer-menu',
						'menu_class'     => 'footer-menu'
					) );
					?>
					</div>
				<?php endif; /* End check if enable footer menu */?>

				<?php if ( get_theme_mod( 'penci_footer_copyright' ) ) : ?>
					<div class="fb">
						<?php $fb = get_field('fb',option) ;?>
						<?php $yt = get_field('yt',option) ;?>
						<a href="<?php echo $fb ;?>" target="_blank"><img src="<?php echo get_stylesheet_directory_uri();?>/images/fb.png" alt=""></a>
						<a href="<?php echo $yt ;?>" target="_blank"><img src="<?php echo get_stylesheet_directory_uri();?>/images/yt.png" alt=""></a>
					</div>
					<div id="footer-copyright">
						<p><?php echo wp_kses( get_theme_mod( 'penci_footer_copyright' ), penci_allow_html() ); ?></p>
					</div>
				<?php endif; ?>
				<?php if ( ! get_theme_mod( 'penci_go_to_top' ) ) : ?>
					<div class="go-to-top-parent"><a href="#" class="go-to-top"><span><i class="fa fa-angle-up"></i><br><?php esc_html_e( 'Back To Top', 'soledad' ); ?></span></a></div>
				<?php endif; ?>
			</div>
		<?php endif; ?>
	</div>
				<!-- popup	-->

		<div id="modalPopup" class="modal-add-article  mfp-hide">
			<div class="penci-border-arrow penci-homepage-title penci-magazine-title popup_bg_color">
				<div class="popup_top_space">
					<button title="Zakmij" type="button" class="mfp-close close_btn">x</button>
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mail_popup.png" alt="">
				</div>
				<div class="popup_container">
					<h4 class="popup_title">Zapisz się do naszego newslettera, </h4>
					<p>by być na bieżąco z technologicznymi nowościami <br>	i najniższymi cenami!</p>
				</div>
				<div class="newsletter_popup">

						<input class="newsletter_input popup_input" type="text" placeholder="Wpisz swój adres Email">
						<input class="newsletter_submit popup_submit" type="submit" value="Wyślij">

				</div>
				
			</div>
		</div>

		<!-- /popup	-->
</footer>

</div><!-- End .wrapper-boxed -->

<div id="fb-root"></div>

<?php wp_footer(); ?>

<?php if( get_theme_mod('penci_footer_analytics') ):
echo get_theme_mod('penci_footer_analytics');
endif; ?>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/inc/popup/dist/jquery.magnific-popup.min.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/inc/jquery-cookie-master/src/jquery.cookie.js"></script>
<script>
(function($) {
	$('.add-article').magnificPopup({
		type: 'inline',
		overflowY: 'scroll',
	});
//			var date = new Date();
//			var date2 = new Date();
// 			date.setTime(date.getTime() + (minutes * 60 * 1000));
// 			date2.setTime(date.getTime() + (minutes * 60 * 1000));
			var delay = Date.now();
			if(!$.cookie('time-visit')) {
				$.cookie('time-visit', delay+120*1000, { expires: 2 }); // Set it to last a year, for example.
			}
		$(window).load(function () {
			var d = Date.now();
			if( d >= ($.cookie('time-visit'))) {
				if($.cookie('popup') != 'seen'){
					$.cookie('popup', 'seen', { expires: 2 }); // Set it to last a year, for example.
					$.magnificPopup.open({
						items: {
							src: '#modalPopup'
						},
						disableOn: 700,
						type: 'inline',
						removalDelay: 160,
						fixedContentPos: false,
						tClose: 'Zamknij',
						tLoading:'Ładowanie...'
					}, 0);
				}
			}
		});
	$(document).on('click', '.modal-dismiss', function (e) {
		e.preventDefault();
		$.magnificPopup.close();
		
	});


}(jQuery));
</script>
</body>
</html>