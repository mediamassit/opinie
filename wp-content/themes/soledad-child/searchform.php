<form role="search" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <div>
		<input type="text" class="search-input" placeholder="<?php esc_html_e('Szukaj...', 'soledad'); ?>" name="s" id="s" />
		<input class="search_submit " type="submit" value="">
	 </div>
</form>