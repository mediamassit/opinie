<?php
/**
 * Socials Widget
 * Get in touch with your clients
 *
 * @package Wordpress
 * @since 1.0
 */

add_action( 'widgets_init', 'my_social_load_widget' );

function my_social_load_widget() {
	register_widget( 'my_social_widget' );
}

class my_social_widget extends WP_Widget {

	/**
	 * Widget setup.
	 */
	function my_social_widget() {
		/* Widget settings. */
		$widget_ops = array( 'classname'   => 'my_social_widget', 'description' => esc_html__( 'A widget that displays your social icons', 'soledad' ) );

		/* Widget control settings. */
		$control_ops = array( 'id_base' => 'my_social_widget' );

		/* Create the widget. */
		global $wp_version;
		if( 4.3 > $wp_version ) {
			$this->WP_Widget( 'my_social_widget', esc_html__( '.My Social Media', 'soledad' ), $widget_ops, $control_ops );
		} else {
			parent::__construct( 'my_social_widget', esc_html__( '.My Social Media', 'soledad' ), $widget_ops, $control_ops );
		}
	}

	/**
	 * How to display the widget on the screen.
	 */
	function widget( $args, $instance ) {
		extract( $args );

		/* Our variables from the widget settings. */
		$title      = apply_filters( 'widget_title', $instance['title'] );
		$text       = $instance['text'];
		$facebook   = $instance['facebook'];
		$twitter    = $instance['twitter'];
		$googleplus = $instance['googleplus'];
		$instagram  = $instance['instagram'];
		$youtube    = $instance['youtube'];
		$video    = $instance['video'];
		$tumblr     = $instance['tumblr'];
		$behance    = $instance['behance'];
		$linkedin   = $instance['linkedin'];
		$flickr     = $instance['flickr'];
		$pinterest  = $instance['pinterest'];
		$email      = $instance['email'];
		$vk         = $instance['vk'];
		$bloglovin  = isset( $instance['bloglovin'] ) ? $instance['bloglovin'] : '';
		$vine       = isset( $instance['vine'] ) ? $instance['vine'] : '';
		$soundcloud = isset( $instance['soundcloud'] ) ? $instance['soundcloud'] : '';
		$snapchat   = isset( $instance['snapchat'] ) ? $instance['snapchat'] : '';
		$spotify   = isset( $instance['spotify'] ) ? $instance['spotify'] : '';
		$rss        = $instance['rss'];

		/* Before widget (defined by themes). */
		echo ent2ncr( $before_widget );

		/* Display the widget title if one was input (before and after defined by themes). */
		if ( $title )
			echo ent2ncr( $before_title . $title . $after_title );

		?>

		<div class="widget-social<?php if( $text ): echo ' show-text'; endif; ?>">
			<?php if ( $video ) : ?>
				<a href="<?php echo get_field('video',option);?>" target="_blank"><div class="social social-video"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/video-w.png" alt=""><h3>Najnowsze wideorelacje</h3></div></a>
			<?php endif; ?>
		
			<?php if ( $youtube ) : ?>
				<a href="<?php echo get_field('yt',option);?>" target="_blank"><div class="social social-yt"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/yt-w.png" alt=""><h3>Odwiedź nasz kanał</h3></div></a>
			<?php endif; ?>
			
			<?php if ( $facebook ) : ?>
			<a href="<?php echo get_field('fb',option);?>" target="_blank" ><div class="social social-fb"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/fb-w.png" alt=""><h3>Odwiedź nasz profil</h3></div></a>
			<?php endif; ?>




		<?php

		/* After widget (defined by themes). */
		echo ent2ncr( $after_widget );
	}

	/**
	 * Update the widget settings.
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Strip tags for title and name to remove HTML (important for text inputs). */
		$instance['title']      = strip_tags( $new_instance['title'] );
		$instance['text']       = strip_tags( $new_instance['text'] );
		$instance['facebook']   = strip_tags( $new_instance['facebook'] );
		$instance['twitter']    = strip_tags( $new_instance['twitter'] );
		$instance['googleplus'] = strip_tags( $new_instance['googleplus'] );
		$instance['instagram']  = strip_tags( $new_instance['instagram'] );
		$instance['linkedin']   = strip_tags( $new_instance['linkedin'] );
		$instance['flickr']     = strip_tags( $new_instance['flickr'] );
		$instance['video']     = strip_tags( $new_instance['video'] );
		$instance['behance']    = strip_tags( $new_instance['behance'] );
		$instance['youtube']    = strip_tags( $new_instance['youtube'] );
		$instance['tumblr']     = strip_tags( $new_instance['tumblr'] );
		$instance['pinterest']  = strip_tags( $new_instance['pinterest'] );
		$instance['email']      = strip_tags( $new_instance['email'] );
		$instance['vk']         = strip_tags( $new_instance['vk'] );
		$instance['bloglovin']  = strip_tags( $new_instance['bloglovin'] );
		$instance['vine']       = strip_tags( $new_instance['vine'] );
		$instance['soundcloud'] = strip_tags( $new_instance['soundcloud'] );
		$instance['snapchat']   = strip_tags( $new_instance['snapchat'] );
		$instance['spotify']   = strip_tags( $new_instance['spotify'] );
		$instance['rss']        = strip_tags( $new_instance['rss'] );

		return $instance;
	}


	function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array(
			'title'      => '',
			'text'       => false,
			'facebook'   => true,
			'twitter'    => 'on',
			'googleplus' => 'on',
			'instagram'  => 'on',
			'linkedin'   => '',
			'behance'    => '',
			'flickr'     => '',
			'youtube'    => '',
			'tumblr'     => '',
			'pinterest'  => 'on',
			'email'      => '',
			'vk'         => '',
			'bloglovin'  => '',
			'vine'       => '',
			'soundcloud' => '',
			'snapchat'   => '',
			'spotify'   => '',
			'rss'        => '',
		);
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e('Title:','soledad'); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" value="<?php echo sanitize_text_field( $instance['title'] ); ?>" />
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'text' ) ); ?>"><?php esc_html_e('Display social text on right icons?:','soledad'); ?></label>
			<input type="checkbox" id="<?php echo esc_attr( $this->get_field_id( 'text' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'text' ) ); ?>" <?php checked( (bool) $instance['text'], true ); ?> />
		</p>

		<p class="description"><?php esc_html_e('Note: Setup your social links in the Appearance -> Customizer','soledad'); ?></p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'facebook' ) ); ?>"><?php esc_html_e('Facebook:','soledad'); ?></label>
			<input type="checkbox" id="<?php echo esc_attr( $this->get_field_id( 'facebook' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'facebook' ) ); ?>" <?php checked( (bool) $instance['facebook'], true ); ?> />
		</p>


		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'youtube' ) ); ?>"><?php esc_html_e('Youtube:','soledad'); ?></label>
			<input type="checkbox" id="<?php echo esc_attr( $this->get_field_id( 'youtube' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'youtube' ) ); ?>" <?php checked( (bool) $instance['youtube'], true ); ?> />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'video' ) ); ?>"><?php esc_html_e('video:','soledad'); ?></label>
			<input type="checkbox" id="<?php echo esc_attr( $this->get_field_id( 'video' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'video' ) ); ?>" <?php checked( (bool) $instance['youtube'], true ); ?> />
		</p>


	<?php
	}
}

?>