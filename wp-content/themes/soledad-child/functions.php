<?php 
//
//include( trailingslashit( get_template_directory() ). 'inc/widgets/my_social_widget.php' );
require_once('inc/widgets/my_social_widget.php');

if ( ! function_exists( 'penci_comments_template' ) ) {
	function penci_comments_template( $comment, $args, $depth ) {
		$GLOBALS['comment'] = $comment;
		?>
		<div <?php comment_class(); ?> id="comment-<?php comment_ID() ?>">
			<div class="thecomment">
				<div class="author-img">
					<?php echo get_avatar( $comment, $args['avatar_size'] ); ?>
				</div>
				<div class="comment-text">
					<span class="author"><?php echo get_comment_author_link(); ?></span>
					<span class="date"><i class=""> |</i><?php printf(  get_comment_date() ) ?></span>
					<?php if ( $comment->comment_approved == '0' ) : ?>
						<em><i class="icon-info-sign"></i> <?php esc_html_e( 'Your comment awaiting approval', 'soledad' ); ?></em>
					<?php endif; ?>
					<div class="comment-content"><?php comment_text(); ?></div>
					<span class="reply">
						<?php comment_reply_link( array_merge( $args, array(
							'reply_text' => esc_html__( 'Reply', 'soledad' ),
							'depth'      => $depth,
							'max_depth'  => $args['max_depth']
						) ), $comment->comment_ID ); ?>
						<?php edit_comment_link( esc_html__( 'Edit', 'soledad' ) ); ?>
					</span>
				</div>
			</div>
	<?php
	}
}



function display_categories($id, $single = false) {
	
	if($single === true) {
		$wpseo_primary_term = new WPSEO_Primary_Term( 'category', $id );
		$wpseo_primary_term = $wpseo_primary_term->get_primary_term();
		$primary_cat = get_term( $wpseo_primary_term );
		
		if(!$primary_cat->errors > 0) {
			$args = array('parent' => $primary_cat->term_id,'orderby' => 'menu_order');
			$categories = get_categories( $args );
		} else {
			$categories = '';
		}
	} else {
		$cat_current = get_query_var('cat');

		$category = get_category($cat_current);
		if($category->parent && $category->parent != 0) {
			$args = array('parent' => $category->parent, 'orderby' => 'menu_order');
		} else {
			$args = array('parent' => $cat_current,'orderby' => 'menu_order');
		}
		$categories = get_categories( $args );
	}
	if($categories && !(sizeof($categories) == 1 && $categories[0]->term_id == $cat_current)) {
		$output .= '<div class="widget widget_categories">';
		$output .= '<h4 class="widget-title penci-border-arrow"><span class="inner-arrow">Kategorie</span></h4>';
		$output .= '<ul>';

		foreach($categories as $cat) {
			$cat_url = get_term_link($cat);
			if($cat_current != $cat->term_id) {
				$output .= '<li class="cat-item cat-item"><a href="'.$cat_url.'">';
				$output .= $cat->name;
				$output .= ' <span class="category-item-count">('.$cat->count.')</span>';
				$output .= '</a></li>';
			}
		}
		$output .= '</ul>';
		$output .= '</div>';
	}
	print $output;
}


function display_main_categories() {
	$args = array('parent' => 0,'orderby' => 'menu_order');
	$categories = get_categories( $args );
	if(!empty($categories)) {
		$output .= '<div class="widget widget_categories">';
		$output .= '<h4 class="widget-title penci-border-arrow"><span class="inner-arrow">'.__('Kategorie',THEME_NAME).'</h4>';
		$output .= '<ul>';

		foreach($categories as $cat) {
			$cat_url = get_term_link($cat);
			if($cat_current != $cat->term_id) {
				$output .= '<li class="cat-item cat-item" ><a href="'.$cat_url.'">';
				$output .= $cat->name;
				$output .= ' <span class="category-item-count">('.$cat->count.')</span>';
				$output .= '</a></li>';
			}
		}
		$output .= '</ul>';
		$output .= '</div>';
	}
	print $output;
}